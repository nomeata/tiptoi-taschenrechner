Ein Taschenrechner für den Tiptoi-Stift
=======================================

Dieses Repository enthält alle Dateien, um sich in drei Schritten einen
Taschenrechner für den Tiptoi-Stift zu basteln.

 1. Man besorgt sich das [tttool](http://tttool.entropia.de/).

 2. Aus der Datei `taschenrechner.yaml` und den Audio-Dateien `*.ogg` erstellt
    der Befehl

        tttool assemble taschenrechner.yaml   

    die Datei `taschenrechner.gme` (die hier auch mitgeliefert wird). Diese
    kopiert man auf den Stift.

 3. Eine der Bilder `taschenrechner.xcf`, `taschenrechner-600.xcf` oder
    `taschenrechner-sw.xcf` druckt man mit einer Auflösung von 1200 resp. 600
    dpi aus.
 

Nicht dass das besonders nützlich ist, aber es zeigt ein weing was man mit dem
eingeschränkten Befehlssatz alles machen kann. 

Das Taschenrechnerbild ist © Joe Haupt, CC-BY-SA, https://flic.kr/p/ehiwxe.
Der Rest ist © 2015 Joachim Breitner, MIT-lizensiert.
   
